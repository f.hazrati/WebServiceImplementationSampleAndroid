package ir.rayanpdm.webservicesample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b = findViewById(R.id.Search_City_Weather_Btn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String cityName = ((EditText)findViewById(R.id.InputCityName)).getText().toString();
                    String webServiceURL = "http://api.openweathermap.org/data/2.5/forecast?q=" + cityName + "&APPID=7eba0261c9de09abed7662f3db59dff3";
                    Log.d("webserviceSampleProj", webServiceURL);

                    AsyncHttpClient client = new AsyncHttpClient();
                    client.get(webServiceURL, new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int i, Header[] headers, String s, Throwable throwable) {
                            Log.d("webserviceSampleProj", s);
                        }

                        @Override
                        public void onSuccess(int ii, Header[] headers, String s) {
                            parse(s);
                        }

                    });
                }
                catch (Exception ex){
                    Log.d("webserviceSampleProj", ex.getMessage());
                }



            }
        });
    }
    void parse(String response) {
        try {
            JSONObject json_data = new JSONObject(response);
            String temperature = new JSONObject(new JSONObject(new JSONArray(json_data.get("list").toString()).get(0).toString()).get("main").toString()).get("temp").toString();

            double k = Double.parseDouble(temperature);

            int c = (int)(k - 273.15d);
            ((TextView)findViewById(R.id.ShowCityWeather)).setText(c + " °C");
        }
        catch(Exception ex){
            Log.d("webserviceSampleProj", ex.getMessage());
        }



        /*JSONObject questionMark = response.getJSONObject("question_mark");
        Iterator keys = questionMark.keys();

        while(keys.hasNext()) {
            // loop to get the dynamic key
            String currentDynamicKey = (String)keys.next();

            // get the value of the dynamic key
            JSONObject currentDynamicValue = questionMark.getJSONObject(currentDynamicKey);

            // do something here with the value...
        }


        result.setText(c + " °C");

*/
    }

}
